(function(window) {

  let header = qs('.header');
  let scrollLimit = window.innerHeight / 2;

  window.addEventListener('resize', e => {
    scrollLimit = window.innerHeight / 2;
  });

  window.addEventListener('scroll', e => {
    let scrolled = window.scrollY;
    
    if (scrolled > scrollLimit) {
      header.classList.remove('hidden');
    }

    else {
      header.classList.add('hidden');
    }
  });

})(window);
