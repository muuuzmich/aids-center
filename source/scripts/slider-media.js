(function(window) {
  let sliders = qsAll('.slider--media');
  

  sliders.forEach(sliderNode => {
    $(sliderNode).append(`
      <button class="next"></button>
      <button class="prev"></button>
    `);

    sliderNode.classList.add('at-start');

    let slidesAmount = $(sliderNode).find('.slide').length - 1;

    const next = qs('.next', sliderNode);
    const prev = qs('.prev', sliderNode);

    let slider = new Flickity(sliderNode, {
      cellAlign:       'center',
      draggable:       true,
      freeScroll:      false,
      wrapAround:      false,
      cellSelector:    '.slide',
      accesibility:    true,
      adaptiveHeight:  false,
      prevNextButtons: false,
      pageDots:        true,
    });

    slider.on('change', function(index) {
      if (index === 0) {
        sliderNode.classList.add('at-start')
      } else {
        sliderNode.classList.remove('at-start')
      }

      if (index === slidesAmount) {
        sliderNode.classList.add('at-end')
      } else {
        sliderNode.classList.remove('at-end')
      }
    });

    slider.on('dragMove', function( event, pointer, moveVector ) {
      if (moveVector.x > 0) {
        prev.classList.add('active');
      } else if (moveVector.x < 0) {
        next.classList.add('active');
      }
    });

    slider.on('dragEnd', function( event, pointer ) {
      next.classList.remove('active');
      prev.classList.remove('active');
    });

    next.addEventListener('click', () => {
      slider.next();
    });

    prev.addEventListener('click', () => {
      slider.previous();
    });
  });
})(window);
