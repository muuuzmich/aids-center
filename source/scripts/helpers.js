function qs(query, container = document) {
  return container.querySelector(query);
}

function qsAll(query, container = document) {
  return [].slice.call(container.querySelectorAll(query));
}
