Конечная сборка в **build**

Шаблоны в **source/templates**

Страница собирается из месяцев.

Класс .content огрничивает сожержимое по ширине. Части сайта н всю ширину должны быть вне его

Примеры оформления верстки в **source/templates/blocks/snippets.twig**
Пример заполнения месяца в **source/templates/blocks/november-2017.twig**

#### Клонирование:
```bash
git clone git@gitlab.com:caspian-seagull/aids-center.git && cd aids-center
```

#### Установка пакетов и запуск:
```bash
npm i -g gulp && npm i && npm run live
```

Сайт запустится по адресу [http://localhost:8000/](http://localhost:8000/)

По вопросам писать на caspian.seagull@gmail.com или в телеграм на @caspianseagull
